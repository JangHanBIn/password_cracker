#include <iostream>
#include <regex>
#include <openssl/evp.h>
#include <list>
#include <unistd.h>
#include <map>
#include "filemanager.h"
#include "cracker.h"
#include "mysqlconnector.h"


using namespace std;

#define ITERATION 500
#define KEY_LEN 16
#define IDX 1
#define ID 2
#define PW 3
#define RAINBOW_FILE "rainbow.bin"
#define RAINBOW_HASH_SIZE 5
#define FILE_OPTIMAZE_NUM 1024 * 97
#define RAINBOW_MODE 1
#define RAINBOW_TO_FILE_MODE 2
#define RAINBOW_SEARCH_MODE 3


namespace D0RK {
int strlen(const char *__s)
{
    uint8_t *c;
    int count = 0;

    while(true)
    {
        c = (uint8_t*)__s++;
        if (*c==NULL)
        {
            break;
        }
        count++;
    }

    return count;
}
}

void Usage(const char * proc_name)
{
    cout<<"Usage : "<< proc_name<< " <password_file> <output_file> <salt>"<<endl;
    exit(1);
}

void MakeRainbow(FileManager &fileManager, const char *salt);
void MakeRainbowFile(FileManager &fileManager, const char* salt);
void SearchRainbow(FileManager &fileManager, map<string,string> &key_chain);
void RainbowToKeyChain(FileManager &fileManager, map<string,string> &key_chain);

int main(int argc, char* argv[])
{

    if (argc !=4)
        Usage(argv[0]);

    const char * input_file_name = argv[1];
    const char * output_file_name = argv[2];
    const char * salt = argv[3];
    FileManager fileManager;


    int mode = RAINBOW_SEARCH_MODE;
    switch (mode) {
    case 1:
    {
        //for make rainbow table by file file format [idx]:[id]:[pw]
        fileManager.set_input_file_name(input_file_name);

        if(!fileManager.InitInputStream())
        {
            cout<<"File Open err! Plz check file path or name."<<endl;
            exit(1);
        }

        MakeRainbow(fileManager, salt);
        break;
    }
    case 2:
    {
        fileManager.set_output_file_name(RAINBOW_FILE);
        if(!fileManager.InitOutputStream())
        {
            cout<<"File Open err! Plz check file path or name."<<endl;
            exit(1);
        }

        MakeRainbowFile(fileManager,salt);
        break;
    }case 3:
    {
        fileManager.set_output_file_name(output_file_name);
        if(!fileManager.InitOutputStream())
        {
            cout<<"File Open err! Plz check file path or name."<<endl;
            exit(1);
        }
        fileManager.set_input_file_name(input_file_name);
        if(!fileManager.InitInputStream())
        {
            cout<<"File Open err! Plz check file path or name."<<endl;
            exit(1);
        }
        fileManager.set_rainbow_file_name(RAINBOW_FILE);
        if(!fileManager.InitRainbowStream())
        {
            cout<<"File Open err! Plz check file path or name."<<endl;
            exit(1);
        }

        map<string, string> key_chain;
        RainbowToKeyChain(fileManager,key_chain);
        SearchRainbow(fileManager, key_chain);
        break;
    }default:
        break;
    }




    return 0;
}



void MakeRainbow(FileManager& fileManager, const char *salt)
{
    //init Cracker for search
    Cracker cracker;
    cracker.RegexAlign(':');

    MysqlConnector mysqlConnector;
    list<std::string> values;

    if(!mysqlConnector.ConnectToServer((char*)"passwd_crack",(char*)"db.dork94.com",(char*)"dork94",(char*)"d0rkd0rk"))
    {
        cout<<"Failed to connect to server. Plz check status."<<endl;
        exit(1);
    }
    mysqlConnector.AddParams((char*)"ID");
    mysqlConnector.AddParams((char*)"PW");
    mysqlConnector.AddParams((char*)"PW_HASHED");
    mysqlConnector.AddParams((char*)"INTER");
    mysqlConnector.AddParams((char*)"SALT");

    string idx;
    string id;
    string pw;

    ostringstream s_hash;

    char buf[512];
    unsigned char pw_hash[KEY_LEN];

    /******************** This code section for make rainbow table and save to DB***********************/
    while(fileManager.ReadLineFromFile(buf,sizeof(buf)))
    {
        string str(buf);
        idx = cracker.FindSome(str,IDX);
        id = cracker.FindSome(str,ID);
        pw = cracker.FindSome(str,PW);

        if (id.empty() || pw.length() < 6)
            continue;

        values.push_back(id);
        values.push_back(pw);

        PKCS5_PBKDF2_HMAC_SHA1(pw.c_str(),pw.length(),(const unsigned char*)salt,D0RK::strlen(salt),ITERATION,16,pw_hash);
        for (auto hexa : pw_hash)
        {
            s_hash<<setfill('0');
            s_hash<<setw(2)<<hex<<(int)hexa;
        }
        values.push_back(s_hash.str());
        values.push_back(to_string(ITERATION));
        values.push_back(salt);
        mysqlConnector.InsertData((char*)"user_info", values);
        s_hash.str(""); //ostringstream must be cleared after init ""
        s_hash.clear();
        values.clear();
    }
    /******************** This code section for make rainbow table and save to DB***********************/

}

void MakeRainbowFile(FileManager &fileManager, const char *salt)
{

    MysqlConnector mysqlConnector;
    list<std::string> values;

    if(!mysqlConnector.ConnectToServer((char*)"passwd_crack",(char*)"db.dork94.com",(char*)"dork94",(char*)"d0rkd0rk"))
    {
        cout<<"Failed to connect to server. Plz check status."<<endl;
        exit(1);
    }

    mysqlConnector.AddParams((char*)"ID");
    mysqlConnector.AddParams((char*)"PW");
    mysqlConnector.AddParams((char*)"PW_HASHED");
    mysqlConnector.AddParams((char*)"INTER");
    mysqlConnector.AddParams((char*)"SALT");

    StoreQueryResult results;
    unsigned char pw_hash[KEY_LEN];
    //Also you can use PW_HASHED field. but i will use PW for later
    values.push_back("PW");
    mysqlConnector.SelectData((char*)"user_info", values,FILE_OPTIMAZE_NUM, results);

    for(int i=0; i<FILE_OPTIMAZE_NUM; i++)
    {

        PKCS5_PBKDF2_HMAC_SHA1(results[i]["PW"], results[i]["PW"].length(),(const unsigned char*)salt,D0RK::strlen(salt),ITERATION,16,pw_hash);
        fileManager.get_write_file()<<results[i]["PW"]<<":";

        //save only rainbow hash size of reduce size
        //and file saved by binary format for reduce size

        for(int i=0; i<RAINBOW_HASH_SIZE;i++)
        {
            fileManager.get_write_file()<<setfill('0');
            fileManager.get_write_file()<<setw(2)<<hex<<(int)pw_hash[i];
        }
        fileManager.get_write_file()<<endl;

    }


}

void SearchRainbow(FileManager &fileManager, map<string, string> &key_chain)
{
    Cracker cracker;
    cracker.RegexAlign(':');

    string str;
    string hash_pw;
    char buf[512];
    map<string, string>::iterator iter;
    while(fileManager.ReadLineFromFile(buf,sizeof(buf)))
    {
        str=buf;
        hash_pw = cracker.FindSome(str, 3);

        fileManager.get_write_file()<<cracker.FindSome(str, 1)<<":"<<cracker.FindSome(str, 2)<<":";


        //find and save node location at iter
        iter = key_chain.find(hash_pw.substr(0, RAINBOW_HASH_SIZE *2));
        if(iter!=key_chain.end())
        {
            //if there is a key
            fileManager.get_write_file()<<iter->second<<endl;
        }else
        {
            fileManager.get_write_file()<<"Unknown"<<endl;
        }

    }

}



void RainbowToKeyChain(FileManager &fileManager, map<string, string> &key_chain)
{
    Cracker cracker;
    string rule = "(.*):(.*)";
    cracker.RegexAlign(rule);


    char buf[512];
    string str;
    string pw;
    string hash_pw;
    while(fileManager.ReadLineFromRainbow(buf, sizeof(buf)))
    {
        str=buf;
        pw = cracker.FindSome(str,1);
        hash_pw = cracker.FindSome(str,2);
        key_chain.insert(pair<string,string>(hash_pw,pw));

    }

}
