#ifndef CRACKER_H
#define CRACKER_H

#include <regex>
#include <iostream>
class Cracker
{
    std::regex r_;
    std::smatch s_;

public:
    Cracker();
    void RegexAlign(char sep);
    void RegexAlign(std::string rule);
    std::string FindSome(std::string &str, int idx);
//    std::string FindSome(char* str, int &idx);
};

#endif // CRACKER_H
