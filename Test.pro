TEMPLATE = app
CONFIG += console c++11
CONFIG += c++11
CONFIG -= app_bundle
CONFIG -= qt

unix:!macx {
    INCLUDEPATH += /usr/include
    INCLUDEPATH += /usr/include/mysql
    LIBS += -L"/usr/include:" -lssl
    LIBS += -L"/usr/include:" -lcrypto

}

macx: {
    INCLUDEPATH += /usr/local/include
    INCLUDEPATH += /usr/local/include/mysql
    LIBS += -L"/usr/local/lib" -lssl
    LIBS += -L"/usr/local/lib" -lcrypto

}



LIBS += -lmysqlpp
LIBS += -lmysqlclient



SOURCES += main.cpp \
    filemanager.cpp \
    cracker.cpp \
    mysqlconnector.cpp

HEADERS += \
    filemanager.h \
    cracker.h \
    mysqlconnector.h
