#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <iostream>
#include <fstream>

class FileManager
{
    const char* input_file_name_=nullptr;
    const char* output_file_name_=nullptr;
    const char* rainbow_file_name_=nullptr;
    std::ofstream write_file_;
    std::ifstream open_file_;
    std::ifstream rainbow_file_;

public:
    FileManager();
    ~FileManager();

    const char* get_input_file_name() const;
    void set_input_file_name(const char *value);
    bool InitOutputStream();
    bool InitInputStream();
    bool InitRainbowStream();
    bool ReadLineFromFile(char *buf, int size_of_buf);
    bool ReadLineFromRainbow(char *buf, int size_of_buf);
    int LinesOfFile();

    std::ofstream &get_write_file();
    const char *get_output_file_name() const;
    void set_output_file_name(const char *value);
    std::ifstream &get_rainbow_file();

    const char *get_rainbow_file_name() const;
    void set_rainbow_file_name(const char *rainbow_file_name);
};



#endif // FILEMANAGER_H
