#ifndef MYSQLCONNECTOR_H
#define MYSQLCONNECTOR_H

#include <mysql++/mysql++.h>
#include <mysql/mysql.h>
#include <iostream>
#include <list>

using namespace mysqlpp;

class MysqlConnector
{
    Connection con_;
    Query query_=con_.query();
    char* db_name_;
    char* server_;
    char* user_;
    char* passwd_;
    std::list<char*> params_;
public:
    MysqlConnector();
    bool ConnectToServer(char* db_name,char* server, char* user, char* passwd);
    void InsertData(char *table_name, std::list<std::string> values);
    void SelectData(char *table_name, std::list<std::string> params, int limit, StoreQueryResult &results);

    char *get_db_name() const;
    void set_db_name(char *get_db_name);
    char *get_server() const;
    void set_server(char *get_server);
    char *user() const;
    void set_user(char *user);
    char *get_passwd() const;
    void set_passwd(char *get_passwd);
    void AddParams(char* param);
};

#endif // MYSQLCONNECTOR_H
