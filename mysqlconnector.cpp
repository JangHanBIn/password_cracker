#include "mysqlconnector.h"

char *MysqlConnector::get_db_name() const
{
    return db_name_;
}

void MysqlConnector::set_db_name(char *db_name)
{
    db_name_ = db_name;
}

char *MysqlConnector::get_server() const
{
    return server_;
}

void MysqlConnector::set_server(char *server)
{
    server_ = server;
}

char *MysqlConnector::user() const
{
    return user_;
}

void MysqlConnector::set_user(char *user)
{
    user_ = user;
}

char *MysqlConnector::get_passwd() const
{
    return passwd_;
}

void MysqlConnector::set_passwd(char *passwd)
{
    passwd_ = passwd;
}

void MysqlConnector::AddParams(char *param)
{
    params_.push_back(param);
}

MysqlConnector::MysqlConnector()
{

}

bool MysqlConnector::ConnectToServer(char *db_name, char *server, char *user, char *passwd)
{
    try{
        con_.connect(db_name,server,user,passwd);

        db_name_= db_name;
        server_ = server;
        user_ = user;
        passwd_ = passwd;

        return true;
    }catch (Exception &e)
    {
        std::cout<<e.what()<<std::endl;
        return false;
    }

}

void MysqlConnector::InsertData(char* table_name, std::list<std::string> values)
{
    // Make parameter to string type
    std::string s_params;
    for (auto param : params_)
    {
        s_params+=" ,";
        s_params+=param;
    }
    s_params = s_params.substr(2); //for remove first space and comma

    std::string s_values;

    for (auto value : values)
    {
        s_values+=" ,\"";
        s_values+=value;
        s_values+='\"';
    }
    s_values = s_values.substr(2); //for remove first space and comma

//    mysql_real_query(con_,query_.str().c_str(),query_.str().length());
    query_<<"INSERT INTO "<< table_name << "(" << s_params<<") "<<"VALUES (";


    for (auto value : values)
    {
        query_<<" \"";
        query_<<escape<<value;
        query_<<"\",";
    }
    // need to modify
    std::string escaped_query = query_.str().substr(0,query_.str().rfind(',')) + ");";
    query_.reset();
    query_<<escaped_query;
    try{
        query_.execute();
//        query_.escape_string(&q,query_.str(),(const char*)query_.str().length());
    }catch(Exception e)
    {
        std::cout<<e.what()<<std::endl;
        std::cout<<query_.str()<<std::endl;
    }




}

void MysqlConnector::SelectData(char *table_name, std::list<std::string> params, int limit, StoreQueryResult& results)
{
    // Make parameter to string type
    std::string s_params;
    for (auto param : params)
    {
        s_params+=" ,";
        s_params+=param;
    }
    s_params = s_params.substr(2); //for remove first space and comma
    //Order for binary search
    query_<<"SELECT "<< s_params << " FROM "<<table_name<<" LIMIT "<< limit<<";";
    // save data to result for processing
    results = query_.store();


}
