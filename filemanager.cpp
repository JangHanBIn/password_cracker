#include "filemanager.h"

const char *FileManager::get_input_file_name() const
{
    return input_file_name_;
}

void FileManager::set_input_file_name(const char *value)
{
    input_file_name_ = value;
}

bool FileManager::InitOutputStream()
{
    if (output_file_name_ == nullptr)
        return false;

    write_file_.open(output_file_name_,std::ios::binary);
    return write_file_.is_open();
}

bool FileManager::InitInputStream()
{
    if (input_file_name_ == nullptr)
        return false;

    open_file_.open(input_file_name_, std::ios::binary);
    return open_file_.is_open();

}

bool FileManager::InitRainbowStream()
{
    if (rainbow_file_name_ == nullptr)
        return false;

    rainbow_file_.open(rainbow_file_name_, std::ios::binary);
    return open_file_.is_open();
}

bool FileManager::ReadLineFromFile(char* buf, int size_of_buf)
{
    if(!open_file_.is_open())
    {
        std::cout<<"ReadLineFromFile must be called after InitInputStream()"<<std::endl;
        exit(1);
    }


    if (open_file_.getline(buf,size_of_buf))
    {
        return true;
    }
    else
        return false;
}

bool FileManager::ReadLineFromRainbow(char *buf, int size_of_buf)
{
    if(!rainbow_file_.is_open())
    {
        std::cout<<"ReadLineFromFile must be called after InitInputStream()"<<std::endl;
        exit(1);
    }


    if (rainbow_file_.getline(buf,size_of_buf))
    {
        return true;
    }
    else
        return false;
}

int FileManager::LinesOfFile()
{
    return std::count(std::istreambuf_iterator<char>(open_file_), std::istreambuf_iterator<char>(), '\n');
}


std::ofstream& FileManager::get_write_file()
{
    return write_file_;
}

const char *FileManager::get_output_file_name() const
{
    return output_file_name_;
}

void FileManager::set_output_file_name(const char *value)
{
    output_file_name_ = value;
}

std::ifstream& FileManager::get_rainbow_file()
{
    return rainbow_file_;
}

const char *FileManager::get_rainbow_file_name() const
{
    return rainbow_file_name_;
}

void FileManager::set_rainbow_file_name(const char *rainbow_file_name)
{
    rainbow_file_name_ = rainbow_file_name;
}

FileManager::FileManager()
{

}

FileManager::~FileManager()
{

    if(write_file_.is_open())
        write_file_.close();
    if(open_file_.is_open())
        open_file_.is_open();
}
